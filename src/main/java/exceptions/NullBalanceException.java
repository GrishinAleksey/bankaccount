package exceptions;

/**
 * Исключение, в случае снятия суммы с нулевого счета
 *
 * <p> Чтобы избежать данное исключение в дальнейшем
 * необходимо убидться что на аккаунте есть деньги.
 *
 */

public class NullBalanceException extends Exception{
    public NullBalanceException(String msg){
        super(msg);
    }
}
