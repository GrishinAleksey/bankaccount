package exceptions;

public class PinLockException extends Exception {
    private final long timeRemaining;

    public PinLockException(long timeRemaining) {
        this.timeRemaining = timeRemaining;
    }

    public String timeRemaining() {
        return String.valueOf(timeRemaining / 1000);
    }
}
