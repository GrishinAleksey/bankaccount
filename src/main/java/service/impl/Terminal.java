package service.impl;

import entity.Client;

public interface Terminal {
    double getMoney(Client client);
    void depositMoney(Client client, double money);
    void withDrawMoney(Client client, double money);
}
