package exceptions;

/**
 * Исключение, в случае превышения суммы снятия над банасом пользователя
 *
 * <p> Чтобы избежить данное исключение необходимо убедиться, что
 * вводимая сумма меньше доступных средств.
 *
 */

public class OutOfBalanceException extends Exception{
    public OutOfBalanceException(String msg){
        super(msg);
    }
}
