package service;

import com.sun.org.slf4j.internal.Logger;
import com.sun.org.slf4j.internal.LoggerFactory;
import entity.Client;
import exceptions.AbsentMultiplyException;
import exceptions.NullBalanceException;
import exceptions.OutOfBalanceException;

public class TerminalServer {

    public static final Logger LOGGER = LoggerFactory.getLogger(TerminalServer.class);

    public void moneyVerification(Client client, double money) throws OutOfBalanceException {
        throw new OutOfBalanceException("OutOfBalanceException");
    }
    public void moneyMultiply(double money) throws AbsentMultiplyException {
        throw new AbsentMultiplyException("AbsentMultiplyException");
    }
    public void nullBalance(Client client) throws NullBalanceException{
        throw new NullBalanceException("NullBalanceException");
    }
}
