package exceptions;

/**
 * Исключение, в случае ввода суммы, не кратной 100.
 *
 * <p> Чтобы избежать данное исключение в дальнейшем
 * перед использованием необходимо проверить сумму
 * на кратность 100.
 *
 */

public class AbsentMultiplyException extends Exception{
    public AbsentMultiplyException(String msg){
        super(msg);
    }
}
