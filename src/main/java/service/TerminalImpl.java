package service;

import com.sun.org.slf4j.internal.Logger;
import com.sun.org.slf4j.internal.LoggerFactory;
import entity.Client;
import exceptions.*;
import service.impl.Terminal;
import validator.PinValidator;

import java.util.UUID;

public class TerminalImpl implements Terminal {

    static Client client = new Client(UUID.randomUUID(), "Сергей", 10000, 1234);
    public static final Logger LOGGER = LoggerFactory.getLogger(TerminalImpl.class);
    static PinValidator pinValidator = new PinValidator();
    private final TerminalServer server = new TerminalServer();



    public void authorize(Client client) throws TerminalException, BadPinException, PinLockException {
        try {
            pinValidator.validate(client);
        } catch (BadPinException e) {
            System.out.println("Неверный пин");
            LOGGER.debug("BadPinException", e);
            authorize(client);
        } catch (PinLockException e) {
            System.out.println("Время до разблокировки: " + e.timeRemaining());
            LOGGER.debug("PinLockException", e);
            authorize(client);
        }
    }

    public void endSession(){
        pinValidator.reset();
    }

    @Override
    public double getMoney(Client client){
        return client.getBalance();
    }

    @Override
    public void depositMoney(Client client, double money) {
        try {
            if(money % 100 != 0) throw new AbsentMultiplyException("AbsentMultiplyException");
        } catch (AbsentMultiplyException absentMultiply) {
            System.out.println("Сумма " + money + " не кратна 100");
            LOGGER.debug("AbsentMultiplyException", absentMultiply);
        }
    }

    @Override
    public void withDrawMoney(Client client, double money) {
        try {
            if(client.getBalance() < money) throw new OutOfBalanceException("OutOfBalanceException");
        } catch (OutOfBalanceException outOfBalance) {
            System.out.println("Баланс клиента '" + client.getFirstName() + "' равен " + client.getBalance() + ", что меньше введенной суммы " + money);
            LOGGER.debug("OutOfBalanceException", outOfBalance);
        }
        try {
            if(money % 100 != 0) throw new AbsentMultiplyException("AbsentMultiplyException");
        } catch (AbsentMultiplyException absentMultiply) {
            System.out.println("Сумма " + money + " не кратна 100");
            LOGGER.debug("AbsentMultiplyException", absentMultiply);
        }
        try {
            if(client.getBalance() == 0) throw new NullBalanceException("NullBalanceException");
        } catch (NullBalanceException nullBalance) {
            System.out.println("Баланс клиента:" + client + " равен 0");
            LOGGER.debug("nullBalanceException", nullBalance);
        }
        client.setBalance(client.getBalance() - money);
    }
}
