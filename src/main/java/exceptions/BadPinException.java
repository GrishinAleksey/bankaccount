package exceptions;

public class BadPinException extends Exception{
    public BadPinException(String msg){
        super(msg);
    }
}
