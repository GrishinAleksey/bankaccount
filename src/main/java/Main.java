import entity.Client;
import exceptions.BadPinException;
import exceptions.PinLockException;
import exceptions.TerminalException;
import service.TerminalImpl;
import validator.PinValidator;

import java.util.Scanner;
import java.util.UUID;

public class Main {

    static Client client = new Client(UUID.randomUUID(), "Сергей", 10000, 1234);
    static TerminalImpl terminal = new TerminalImpl();
    static PinValidator pinValidator = new PinValidator();


    public static void main(String[] args) throws TerminalException, BadPinException, PinLockException {
        Scanner scanner = new Scanner(System.in);
        while(true){
            terminal.authorize(client);
            boolean manage = true;

            while (manage){

                System.out.println("Выберите операцию с теминалом");
                System.out.println("1 - проверить баланс");
                System.out.println("2 - снять наличные");
                System.out.println("3 - положить наличные");
                System.out.println("4 - завершить обслуживание");

                int in = scanner.nextInt();
                switch (in) {
                    case 1:
                        System.out.println(terminal.getMoney(client));
                        break;
                    case 2:
                        terminal.withDrawMoney(client, 12000);
                        break;
                    case 3:
                        terminal.depositMoney(client, 2222);
                        break;
                    case 4:
                        manage = false;
                        break;
                }
            }


            System.out.println("Завершаем обслуживание");
            terminal.endSession();
        }
    }
}
